package com.chisw.instaitemsample.model

/**
 * Created by Alex Kriatov on 2019-08-03
 */

data class Post(
    val id:String,
    val owner: User,
    val location: String,
    val likesCount: Int,
    val photos: List<String>)