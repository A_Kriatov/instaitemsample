package com.chisw.instaitemsample.model

/**
 * Created by Alex Kriatov on 2019-08-03
 */

data class User( val fullName: String,
                 val name: String,
                 val id: String,
                 val avatar: String?)