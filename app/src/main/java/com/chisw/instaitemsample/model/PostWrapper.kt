package com.chisw.instaitemsample.model

/**
 * Created by Alex Kriatov on 2019-08-03
 */
data class PostWrapper(val post: Post, var currentItem: Int = 0)