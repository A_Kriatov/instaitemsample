package com.chisw.instaitemsample.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.chisw.instaitemsample.R
import com.chisw.instaitemsample.common.loadCircleImage
import com.chisw.instaitemsample.common.visibleIf
import com.chisw.instaitemsample.model.Post
import com.chisw.instaitemsample.model.PostWrapper
import kotlinx.android.synthetic.main.item_post.view.*

/**
 * Created by Alex Kriatov on 2019-08-03
 */
class PostsRecyclerAdapter : RecyclerView.Adapter<PostsRecyclerAdapter.PostViewHolder>() {

    private var data : List<PostWrapper>? = null

    private var listener: OnPostClickListener? = null

    fun setData(items: List<PostWrapper>?){
        data = items
        notifyDataSetChanged()
    }

    fun setClickListener(listener: OnPostClickListener?){
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
       return PostViewHolder(view)
    }

    override fun getItemCount() = data?.size ?: 0

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        data?.get(position)?.let { holder.bind(it) }
    }

    inner class PostViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(binding: PostWrapper){
            itemView.ivAvatar.loadCircleImage(binding.post.owner.avatar)
            itemView.tvName.text = binding.post.owner.fullName
            itemView.tvLocation.text = binding.post.location
            setupRecycler(binding)
            setListeners()
        }

        private fun setListeners(){
            itemView.ivLike.setOnClickListener {
                data?.get(adapterPosition)?.let { listener?.onPostLikeClicked(it.post) }
            }
            itemView.ivComment.setOnClickListener {
                data?.get(adapterPosition)?.let { listener?.onPostCommentClicked(it.post) }
            }
            itemView.ivSend.setOnClickListener {
                data?.get(adapterPosition)?.let { listener?.onPostSendClicked(it.post) }
            }
            itemView.ivBookmark.setOnClickListener {
                data?.get(adapterPosition)?.let { listener?.onPostBookmarkClicked(it.post) }
            }
        }

        private fun setupRecycler(binding: PostWrapper){
            val adapter = ImagesRecyclerAdapter()
            itemView.recyclerPhoto.adapter = adapter
            itemView.recyclerPhoto.layoutManager = LinearLayoutManager(itemView.context, RecyclerView.HORIZONTAL, false)
            val snapHelper = PagerSnapHelper()
            itemView.recyclerPhoto.attachSnapHelperWithListener(snapHelper,
                SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL,object : OnSnapPositionChangeListener{
                    override fun onSnapPositionChange(position: Int) {
                        data?.get(adapterPosition)?.let {
                            it.currentItem = position
                            moveIndicator(it)
                        }
                    }
                })
            adapter.setData(binding.post.photos)
            itemView.recyclerPhoto.scrollToPosition(binding.currentItem)
            moveIndicator(binding)
        }
        private fun moveIndicator(binding: PostWrapper){
            itemView.indicator?.apply {
                count = binding.post.photos.size
                this.visibleIf { count > 1 }
                selection = binding.currentItem
            }
        }
    }

    interface OnPostClickListener{
        fun onPostLikeClicked(post: Post)
        fun onPostCommentClicked(post: Post)
        fun onPostSendClicked(post: Post)
        fun onPostBookmarkClicked(post: Post)
    }
}