package com.chisw.instaitemsample.repository

import android.content.Context
import android.util.Log
import com.chisw.instaitemsample.model.Post
import com.chisw.instaitemsample.model.User
import com.google.gson.Gson
import java.io.IOException

/**
 * Created by Alex Kriatov on 2019-08-03
 */
interface PostsRepository {
    fun loadPosts(): List<Post>
}

class PostsRepositoryImpl(private val gson: Gson, private val context: Context) : PostsRepository {

    override fun loadPosts(): List<Post> {
//        val posts = loadJSONFromAsset()
//        return gson.fromJson(posts, object : TypeToken<List<Post>>() {}.type)
        val res = getMockPosts()
        Log.d("qqq", gson.toJson(res))
        return res
    }


    private fun loadJSONFromAsset(): String? {
        var json: String? = null
        try {
            val inputStream = context.assets.open("posts.json")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            json = String(buffer, java.nio.charset.StandardCharsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }

        return json
    }

    private fun getMockPosts(): List<Post> {
        val list = ArrayList<Post>()
        list.add(
            Post(
                "1",
                getHH(),
                "Kharkiv",
                33,
                listOf(
                    "https://instagram.fhrk2-1.fna.fbcdn.net/vp/e2d8fbbf0e20cb7143c3dd43cb9493df/5DD5F179/t51.2885-15/e35/65954525_2452855811670867_1249696057991922856_n.jpg?_nc_ht=instagram.fhrk2-1.fna.fbcdn.net",
                    "https://instagram.fhrk2-1.fna.fbcdn.net/vp/f293fcdec97b588493fc8b0560e4e183/5DD35A15/t51.2885-15/e35/65753617_1621820977951175_3109294851709076222_n.jpg?_nc_ht=instagram.fhrk2-1.fna.fbcdn.net"
                )
            )
        )

        list.add(
            Post(
                "2",
                getAfisha(),
                "Kharkiv",
                33,
                listOf("https://instagram.fhrk2-1.fna.fbcdn.net/vp/a748f9ee816063d8d35c3c0da816a488/5DE211D0/t51.2885-15/e15/65618612_2341255756115700_3644469319716116296_n.jpg?_nc_ht=instagram.fhrk2-1.fna.fbcdn.net")
            )
        )

        list.add(
            Post(
                "3",
                getHH(),
                "Kharkiv",
                33,
                listOf(
                    "https://instagram.fhrk2-1.fna.fbcdn.net/vp/942d6d4de18bceb85e805b3273571fcf/5DDDC777/t51.2885-15/e35/66226128_375421696477615_7184278260494099632_n.jpg?_nc_ht=instagram.fhrk2-1.fna.fbcdn.net",
                    "https://instagram.fhrk2-1.fna.fbcdn.net/vp/84e534db2270a012f9bd041badcc65f5/5DE17108/t51.2885-15/e35/66476678_906920882995758_6892503070456826546_n.jpg?_nc_ht=instagram.fhrk2-1.fna.fbcdn.net"
                )
            )
        )
        return list
    }

    private fun getHH(): User {
        return User(
            fullName = "Хуевый Харьков",
            name = "h_kharkov",
            id = "3305407954",
            avatar = getHHPic()
        )
    }

    private fun getAfisha(): User {
        return User(
            fullName = "Kharkiv Event | Афиша Харькова",
            name = "kharkiv.event",
            id = "3019954575",
            avatar = getAfishaPick()
        )
    }

    private fun getHHPic(): String {
        return "https://instagram.fhrk2-1.fna.fbcdn.net/vp/197ddfe22eaa0ca7c83a2797d488a3db/5DE42949/t51.2885-19/s150x150/60749123_539821113214484_3131399563898781696_n.jpg?_nc_ht=instagram.fhrk2-1.fna.fbcdn.net"
    }

    private fun getAfishaPick(): String {
        return "https://instagram.fhrk2-1.fna.fbcdn.net/vp/4167dcec7619b526e5a07482fbafbba3/5DD7F2BE/t51.2885-19/s150x150/52348590_396909807534806_4094300385206861824_n.jpg?_nc_ht=instagram.fhrk2-1.fna.fbcdn.net"
    }
}