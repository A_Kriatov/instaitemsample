package com.chisw.instaitemsample.base

import com.chisw.instaitemsample.repository.PostsRepository

/**
 * Created by Alex Kriatov on 2019-08-03
 */
interface Injection {
    fun injectPostsRepository(): PostsRepository
}