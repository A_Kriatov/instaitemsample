package com.chisw.instaitemsample.base

import android.app.Application
import com.chisw.instaitemsample.repository.PostsRepository
import com.chisw.instaitemsample.repository.PostsRepositoryImpl
import com.google.gson.Gson

/**
 * Created by Alex Kriatov on 2019-08-03
 */
class App: Application(), Injection {

    private val postsRepository: PostsRepository by lazy { PostsRepositoryImpl(Gson(),this) }

    override fun injectPostsRepository(): PostsRepository {
        return postsRepository
    }
}