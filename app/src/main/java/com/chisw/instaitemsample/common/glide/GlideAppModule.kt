package com.chisw.instaitemsample.common.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by Alex Kriatov on 2019-08-03
 */

@GlideModule
class GlideAppModule : AppGlideModule()